import { CButton, CModal, CModalHeader, CModalBody, CModalFooter, CModalTitle, CTable, CTableHead, CTableHeaderCell, CTableRow, CTableBody, CTableDataCell, CRow, CCol } from '@coreui/react';

import React, { useEffect, useState } from 'react';

function ModalDetail({ openModal, closeModal, orderDetails }) {
    const fetchAPI = async (url, body) => {
        let response = await fetch(url, body)
        let data = await response.json()
        return data
    }
    const [orders, setOrders] = useState([]);
    const [allOrders, setAllOrders] = useState([]);
    const getAllOrders = () => {
        fetchAPI(`http://localhost:8000/orders/`)
            .then((data) => {
                setAllOrders(data.data)
            })
            .catch((error) => {
                console.error(error.message)
            })
    }

    const getOrders=()=>{
        let orderArr=[];
        orderDetails.orders.forEach(orderId => {
            let orderInfo = allOrders.filter((order, index) => {
                return order._id === orderId
            })
            orderArr=[...orderArr, orderInfo[0]];
        });
        return orderArr;
    }
    function priceFormat(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    }
    useEffect(() => {
        getAllOrders();
    }, [])

    useEffect(() => {
        if (orderDetails) {
            setOrders(getOrders());
        }
    }, [openModal])
    return (
        <>
            <CModal visible={openModal} onClose={closeModal} backdrop="static" size='lg'>
                <CModalHeader onClose={closeModal}>
                    <CModalTitle>Chi tiết đơn hàng của số điện thoại: {orderDetails ? orderDetails.phone : null} </CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <CTable>
                        <CTableHead>
                            <CTableRow>
                                <CTableHeaderCell scope="col">Id đơn hàng</CTableHeaderCell>
                                <CTableHeaderCell scope="col">Sản phẩm</CTableHeaderCell>
                                <CTableHeaderCell scope="col">Tổng tiền</CTableHeaderCell>

                            </CTableRow>
                        </CTableHead>
                        <CTableBody>
                            {
                                // orderDetails ?
                                //     orderDetails.orders.map((order, index) => {
                                //         return (
                                //             <CTableRow key={index}>
                                //                 <CTableDataCell>{order}</CTableDataCell>


                                //             </CTableRow>
                                //         )
                                //     }) : null

                                orders ?
                                    orders.map((order, index) => {
                                        return (
                                            <CTableRow key={index}>
                                                <CTableDataCell>{order._id}</CTableDataCell>
                                                <CTableDataCell>
                                                    {
                                                        order.orderDetail.map((item, ind)=>(
                                                            <div key={ind}>
                                                             <p>{item.name} x {item.quantity}</p>
                                                            </div>
                                                        ))
                                                    }
                                                </CTableDataCell>
                                                <CTableDataCell>{priceFormat(order.cost)} VND</CTableDataCell>
                                            </CTableRow>
                                        )
                                    }) : null
                            }
                        </CTableBody>
                    </CTable>
                </CModalBody>
                <CModalFooter>
                    <CButton color="secondary" onClick={closeModal}>
                        Close
                    </CButton>
                </CModalFooter>
            </CModal>
        </>
    )

}
export default ModalDetail;